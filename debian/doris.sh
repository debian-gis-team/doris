#!/bin/sh

set -e

export LC_TIME=C
export LC_NUMERIC=C

DORISBIN=/usr/lib/doris
GMTROOT=/usr/lib/gmt

export PATH=${PATH}:${DORISBIN}:${GMTROOT}/bin

exec ${DORISBIN}/doris "$@"
